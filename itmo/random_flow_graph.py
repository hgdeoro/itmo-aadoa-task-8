import random
import typing

from itmo.graph import Edge


class RandomFlowGraphGenerator:
    def __init__(self, random: random.Random, rows: int, cols: int, edges_per_col: int):
        self._random = random
        self._rows = rows
        self._cols = cols
        self._edges_per_col = edges_per_col
        self._n_vertices = (rows * cols) + 2
        self._source_vertex = (rows * cols)
        self._sink_vertex = (rows * cols) + 1
        self._vertices: typing.List[int] = []
        self._edges: typing.Set[Edge] = set()
        self._pos: dict = {}

    @property
    def source_vertex(self):
        return self._source_vertex

    @property
    def sink_vertex(self):
        return self._sink_vertex

    @property
    def edges(self):
        return self._edges

    @property
    def pos(self):
        return self._pos

    def _create_edge(self, v1, v2) -> Edge:
        return Edge(
            source_vertex=v1,
            target_vertex=v2,
            capacity=float(self._random.randint(10, 20)),
        )

    def _create_random_edge(self, prev_column, cur_column):
        for _ in range(100):
            new_edge = self._create_edge(
                self._random.choice(prev_column),
                self._random.choice(cur_column)
            )
            if new_edge not in self._edges:
                return new_edge
        raise Exception("Couldn't create random edge after many attempts")

    def generate(self):
        cols = []
        pos = {
            self._source_vertex: [-3, self._rows / 2.0],
            self._sink_vertex: [self._cols * 3, self._rows / 2.0],
        }
        for col in range(self._cols):
            cur_column = [(col * self._rows) + row
                          for row in range(self._rows)]

            for vertex in cur_column:
                pos[vertex] = [col * 3, vertex % self._rows]

            if not cols:
                cols.append(cur_column)
                continue  # first iteration, there is no 'previous' column

            prev_column = cols[-1]
            self._random.shuffle(prev_column)
            self._random.shuffle(cur_column)

            edges_to_add = self._edges_per_col
            assert edges_to_add >= 1

            # First try to connect, at least one, as much vertices as possible
            for prev, cur in zip(prev_column, cur_column):
                self._edges.add(self._create_edge(prev, cur))
                edges_to_add -= 1
                if edges_to_add <= 0:
                    break

            assert edges_to_add >= 0

            # Now let's connect random vertices
            while edges_to_add > 0:
                self._edges.add(
                    self._create_random_edge(prev_column, cur_column))
                edges_to_add -= 1

            cols.append(cur_column)

        for vertex in cols[0]:
            self._edges.add(self._create_edge(self._source_vertex, vertex))

        for vertex in cols[-1]:
            self._edges.add(self._create_edge(vertex, self._sink_vertex))

        self._pos = pos
