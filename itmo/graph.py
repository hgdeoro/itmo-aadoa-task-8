import dataclasses
import logging
import pathlib
import random
import typing

import matplotlib.pyplot as plt
import networkx as nx


@dataclasses.dataclass(order=True, unsafe_hash=True)
class Edge:
    source_vertex: int = dataclasses.field(compare=True, hash=True)
    target_vertex: int = dataclasses.field(compare=True, hash=True)
    capacity: float = dataclasses.field(compare=False, hash=False)
    flow: float = dataclasses.field(compare=False, hash=False, default=0.0)
    reverse: bool = dataclasses.field(compare=False, hash=False, default=False)
    linked: 'Edge' = dataclasses.field(compare=False, hash=False, default=None)

    def __post_init__(self):
        if self.reverse:
            # We use capacity from 'linked'
            assert self.capacity == 0
            assert self.linked

    def create_reverse(self):
        assert not self.reverse
        assert not self.linked
        reverse = Edge(
            source_vertex=self.target_vertex,
            target_vertex=self.source_vertex,
            capacity=0.0,
            flow=0.0,
            reverse=True,
            linked=self
        )
        self.linked = reverse
        return reverse

    def residual_capacity(self):
        if self.reverse:
            return self.linked.flow
        else:
            return self.capacity - self.flow

    def fc(self):
        return f"f={self.flow:.1f}/c={self.capacity:.1f}"


@dataclasses.dataclass
class Params:
    seed: int
    n_vertices: int
    n_egdes: int
    edges: typing.List[Edge] = None
    source_vertex: int = None
    sink_vertex: int = None
    vis_pos: dict = None
    vis_options: dict = None
    vis_edge_options: dict = None

    def __post_init__(self):
        if self.edges:
            assert self.n_egdes == 0
            assert self.source_vertex is not None
            assert self.sink_vertex is not None
            self.n_egdes = len(self.edges)

            if self.n_vertices is None:
                vertices = [edge.source_vertex for edge in self.edges] + \
                           [edge.target_vertex for edge in self.edges]
                self.n_vertices = len(set(vertices))


class DataWrapper:
    def __init__(self, params: Params):
        self.params: Params = params
        self.random = random.Random(self.params.seed)
        self.adjacency_list: typing.List[typing.Set[Edge]] = []
        self.graph = None

    def edges(self):
        """Return NON reverse edges"""
        for edges_set in self.adjacency_list:
            for edge in edges_set:
                if not edge.reverse:
                    yield edge

    def reverse_edges(self):
        """Return ONLY reverse edges"""
        for edges_set in self.adjacency_list:
            for edge in edges_set:
                if edge.reverse:
                    yield edge

    def all_edges(self):
        """Return ALL (reverse and original flow-network) edges"""
        for edges_set in self.adjacency_list:
            for edge in edges_set:
                yield edge

    def create_reverse_edges(self):
        assert not list(self.reverse_edges())
        for edge in list(self.edges()):
            reverse = edge.create_reverse()
            self.adjacency_list[reverse.source_vertex].add(reverse)

    def _create_random_edge(self) -> Edge:
        for i in range(1000):  # try 1000 times
            v1 = self.random.randint(0, self.params.n_vertices - 1)
            v2 = self.random.randint(0, self.params.n_vertices - 1)
            if v1 == v2:  # self: ignore
                continue
            edge = Edge(v1, v2)
            if edge in self.adjacency_list[v1]:
                continue  # already exists: ignore

            self.adjacency_list[v1].add(edge)
            return edge

        raise Exception(f"Couldn't create edge after {i + 1} attempts")

    def load_graph_from_params(self):
        assert not self.adjacency_list
        self.adjacency_list = [set() for _ in range(self.params.n_vertices)]
        for edge in self.params.edges:
            self.adjacency_list[edge.source_vertex].add(edge)
        return self

    def create_networkx_graph(self):
        assert self.graph is None
        assert self.adjacency_list is not None
        graph = nx.DiGraph()
        for edges in self.adjacency_list:
            for edge in edges:
                graph.add_edge(edge.source_vertex,
                               edge.target_vertex,
                               weight=edge.capacity)
        self.graph = graph

    def plot(self, output_filename=None):
        assert self.graph
        output_filename = output_filename or f'data/graph-{self.params.seed}.pdf'
        if not pathlib.Path(output_filename).exists():
            fig_data, ax_data = plt.subplots()
            ax_data.axis("off")
            pos = self.params.vis_pos or nx.spring_layout(self.graph,
                                                          iterations=1000)
            options = dict(
                with_labels=False,
                node_size=80,
                width=0.5,
                font_size=6,
                font_color='#eee',
                alpha=0.7)
            if self.params.vis_options:
                options.update(self.params.vis_options)
            nx.draw_networkx(
                self.graph,
                pos,
                ax=ax_data,
                edgelist=[(edge.source_vertex, edge.target_vertex)
                          for edge in self.edges()],
                **options,
            )
            # edge_labels = {(edge.source_vertex, edge.target_vertex): edge.fc()
            #                for edge in self.edges()}
            #
            # vis_edge_options = dict(font_size=8)
            # vis_edge_options.update(self.params.vis_edge_options)
            #
            # nx.draw_networkx_edge_labels(self.graph,
            #                              pos,
            #                              edge_labels=edge_labels,
            #                              **vis_edge_options)
            logging.info("Wrinting plot to %s", output_filename)
            fig_data.savefig(output_filename)
        else:
            logging.warning("Won't write plot, file exists: %s", output_filename)
