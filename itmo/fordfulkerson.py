import collections
import logging
import typing

from itmo.graph import DataWrapper, Edge

PREV_EMPTY = -1
INF = float('inf')


class FordFulkerson:
    def __init__(self,
                 data_wrapper: DataWrapper,
                 max_iterations: int = None,
                 after_iteration_callback: typing.Callable = None):
        self.data_wrapper = data_wrapper
        self.prev: typing.List[int] = None
        self.after_iteration_callback: typing.Callable = after_iteration_callback
        self.max_iterations = max_iterations or 1000

        self._reset()

    def _reset(self):
        for edge in self.data_wrapper.edges():
            edge.flow = 0.0
        self.prev = [PREV_EMPTY for _ in range(self.data_wrapper.params.n_vertices)]

    def _bfs(self) -> typing.List[int]:
        for i in range(len(self.prev)):
            self.prev[i] = PREV_EMPTY
        visited_vertices: typing.Set[int] = set()
        queue = collections.deque()
        queue.append(self.data_wrapper.params.source_vertex)  # enqueue

        while queue:
            cur: int = queue.popleft()  # dequeue
            logging.debug("_calculate_augmenting_path() cur=%s - queue=%s", cur, queue)
            visited_vertices.add(cur)

            for edge in self.data_wrapper.adjacency_list[cur]:
                vertex_next = edge.target_vertex
                if (edge.residual_capacity() > 0.0
                        and vertex_next not in visited_vertices):
                    queue.append(vertex_next)  # enqueue
                    visited_vertices.add(vertex_next)
                    self.prev[vertex_next] = cur

        if self.prev[self.data_wrapper.params.sink_vertex] == PREV_EMPTY:
            return []

        path_vertices = []
        path_vertex_i = self.data_wrapper.params.sink_vertex
        while path_vertex_i != PREV_EMPTY:
            path_vertices.insert(0, path_vertex_i)
            path_vertex_i = self.prev[path_vertex_i]

        return path_vertices

    def _create_path_from_vertices(
            self, vertices: typing.List[int]) -> typing.List[Edge]:
        edges = list([None] * (len(vertices) - 1))
        for i in range(len(vertices) - 1):
            cur_vertex = vertices[i]
            next_vertex = vertices[i + 1]
            for edge in self.data_wrapper.adjacency_list[cur_vertex]:
                if edge.target_vertex == next_vertex:
                    edges[i] = edge
                    break
            assert edges[i]
        return edges

    def _calculate_augmenting_path(self):
        vertices = self._bfs()
        maybe_augmenting_path = self._create_path_from_vertices(vertices)
        min_residual_capacity = INF
        for edge in maybe_augmenting_path:
            rc = edge.residual_capacity()
            if rc < min_residual_capacity:
                min_residual_capacity = rc

        if min_residual_capacity == INF:
            return 0, []
        else:
            return min_residual_capacity, maybe_augmenting_path

    def run_method(self):
        for _ in range(self.max_iterations):
            min_rc, augmenting_path = self._calculate_augmenting_path()
            if not augmenting_path:
                return
            for edge in augmenting_path:
                if edge.reverse:
                    # cancellation
                    edge.linked.flow -= min_rc
                else:
                    # augmentation
                    edge.flow += min_rc

            if self.after_iteration_callback:
                self.after_iteration_callback(
                    augmenting_path,
                )
        raise Exception("COULDN'T FIND OPTIMAL")
