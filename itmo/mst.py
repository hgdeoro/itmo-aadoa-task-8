import dataclasses
import logging
import random
import typing

import matplotlib.pyplot as plt
import networkx as nx

import pqdict

INF = float('inf')

SOURCE = 0
TARGET = 1
WEIGHT = 2


@dataclasses.dataclass
class Params:
    seed: int
    n_vertices: int
    n_egdes: int
    mst_root: int
    edges: typing.List[typing.Tuple] = None

    def __post_init__(self):
        if self.edges:
            assert self.n_egdes == 0
            assert self.n_vertices == 0
            self.n_egdes = int(len(self.edges) / 2)

            self.n_vertices = len(set([edge[SOURCE] for edge in self.edges] +
                                      [edge[TARGET] for edge in self.edges]))
        else:
            assert self.n_egdes > 0
            assert self.n_vertices > 0


class DataWrapper:
    def __init__(self, params: Params):
        self.params: Params = params
        self.random = random.Random(self.params.seed)
        self.adjacency_list: typing.List[typing.Tuple] = []
        self.graph = None

    def _random_weight(self):
        return self.random.randint(0, 20)

    def _create_random_edge(self):
        for i in range(1000):  # try 1000 times
            v1 = self.random.randint(0, self.params.n_vertices - 1)
            v2 = self.random.randint(0, self.params.n_vertices - 1)
            if v1 == v2:  # self: ignore
                continue
            if v2 in self.adjacency_list[v1] or v1 in self.adjacency_list[v2]:
                continue  # already exists: ignore

            weight = self._random_weight()
            self.adjacency_list[v1].append((v1, v2, weight))
            self.adjacency_list[v2].append((v2, v1, weight))
            return

        raise Exception(f"Couldn't create edge after {i + 1} attempts")

    def create_random_complete_graph(self):
        assert not self.adjacency_list
        assert not self.params.edges

        assert self.params.n_egdes >= self.params.n_vertices - 1

        # Build adjacency list
        self.adjacency_list = [[] for _ in range(self.params.n_vertices)]

        # first connect all the vertices
        pending_vertices = list(range(self.params.n_vertices))
        self.random.shuffle(pending_vertices)

        connected_vertices = [pending_vertices.pop()]

        while pending_vertices:
            weight = self._random_weight()
            v1 = connected_vertices[-1]
            v2 = pending_vertices.pop()
            self.adjacency_list[v1].append((v1, v2, weight))
            self.adjacency_list[v2].append((v2, v1, weight))
            connected_vertices.append(v2)

        # so far, we created (n_vertices - 1) edges... now add random edges
        for _ in range(self.params.n_egdes - (self.params.n_vertices - 1)):
            self._create_random_edge()

    def load_graph_from_params(self):
        assert not self.adjacency_list
        assert self.params.edges

        # Build adjacency list
        self.adjacency_list = [[] for _ in range(self.params.n_vertices)]

        # Load existing edges
        for edge in self.params.edges:
            self.adjacency_list[edge[SOURCE]].append(edge)
        return self

    def create_networkx_graph(self):
        assert self.graph is None
        assert self.adjacency_list is not None
        graph = nx.Graph()
        for edges in self.adjacency_list:
            for edge in edges:
                if edge[SOURCE] <= edge[TARGET]:
                    graph.add_edge(edge[SOURCE],
                                   edge[TARGET],
                                   weight=edge[WEIGHT])
        self.graph = graph

    def _create_networkx_graph_mst(self, mst_edges):
        assert self.adjacency_list is not None
        graph = nx.Graph()
        for edge in mst_edges:
            graph.add_edge(edge[SOURCE],
                           edge[TARGET],
                           weight=edge[WEIGHT])
        return graph

    def plot(self, mst_edges, output_filename=None):
        assert self.graph

        # create mst graph
        unique_edges = []
        for edge_list in self.adjacency_list:
            for edge in edge_list:
                if edge[SOURCE] < edge[TARGET]:
                    unique_edges.append(edge)

        mst_graph = self._create_networkx_graph_mst(mst_edges)
        mst_graph_pos = nx.spring_layout(mst_graph,
                                         iterations=1000,
                                         weight=None,
                                         seed=1)

        output_filename = output_filename or f'data/mst-{self.params.seed}.pdf'

        fig_data, ax_data = plt.subplots()
        ax_data.axis("off")

        options = dict(
            with_labels=False,
            node_size=80,
            width=0.5,
            font_size=6,
            font_color='#eee',
            alpha=0.4)

        nx.draw_networkx(
            self.graph,
            mst_graph_pos,
            ax=ax_data,
            edgelist=[(edge[SOURCE], edge[TARGET])
                      for edge in unique_edges],
            **options,
        )

        nx.draw_networkx_edges(
            self.graph,
            mst_graph_pos,
            edgelist=[(edge[SOURCE], edge[TARGET])
                      for edge in mst_edges],
            width=2,
        )

        logging.info("Wrinting plot to %s", output_filename)
        fig_data.savefig(output_filename)


def mst(data_wrapper: DataWrapper):
    parents = [(None, None) for _ in range(data_wrapper.params.n_vertices)]
    pq = pqdict.pqdict({v: INF for v in range(data_wrapper.params.n_vertices)})
    pq[data_wrapper.params.mst_root] = 0

    while pq:
        vertex, weight = pq.popitem()

        for adj_edge in data_wrapper.adjacency_list[vertex]:
            curr_weight = pq.get(adj_edge[TARGET], None)
            if curr_weight is not None and adj_edge[WEIGHT] < curr_weight:
                pq[adj_edge[TARGET]] = adj_edge[WEIGHT]
                parents[adj_edge[TARGET]] = (vertex, adj_edge[WEIGHT])

    mst_edges = {
        (v, target_WEIGHT[0], target_WEIGHT[1])
        for v, target_WEIGHT in enumerate(parents)
        if v != data_wrapper.params.mst_root
    }

    return mst_edges
