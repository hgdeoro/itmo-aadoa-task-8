import dataclasses
import gc
import logging
import math
import pathlib
import sys
import time
import typing

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from scipy import optimize

from itmo.mst import Params, DataWrapper, mst


class Main:
    def __init__(self, params: Params):
        logging.debug("params=%s", params)
        self.params: Params = params
        self.data_wrapper: DataWrapper = None

    def setup(self):
        logging.info("---------- setup()")
        assert self.params is not None
        assert self.data_wrapper is None
        self.data_wrapper = DataWrapper(self.params)
        if self.params.edges:
            self.data_wrapper.load_graph_from_params()
        else:
            self.data_wrapper.create_random_complete_graph()
        self.data_wrapper.create_networkx_graph()


def setup_logging(level=logging.INFO):
    logging.basicConfig(level=level, stream=sys.stdout)
    logging.getLogger('matplotlib').setLevel(logging.WARNING)


def main_sample_from_book():
    SOURCE = 0
    TARGET = 1
    WEIGTH = 2

    a, b, c, d, e, f, g, h, i = 0, 1, 2, 3, 4, 5, 6, 7, 8
    edges = {
        (a, b, 4),
        (a, h, 8),
        (b, h, 11),
        (b, c, 8),
        (c, i, 2),
        (c, f, 4),
        (c, d, 7),
        (d, e, 9),
        (d, f, 14),
        (e, f, 10),
        (f, g, 2),
        (g, h, 1),
        (g, i, 6),
        (h, i, 7),
    }

    for f_edge in list(edges):
        edges.add((f_edge[TARGET], f_edge[SOURCE], f_edge[WEIGTH]))

    root_vertex = list(edges)[0][SOURCE]

    main = Main(
        Params(
            seed=1,
            n_vertices=0,
            n_egdes=0,
            edges=edges,
            mst_root=root_vertex,
        )
    )
    main.setup()
    mst(data_wrapper=main.data_wrapper)


@dataclasses.dataclass
class Result:
    n_vertices: int
    n_edges: int
    runtimes_ms: typing.List[float]


def main_performance_test(sizes, repeat=5) -> typing.List[Result]:
    results: typing.List[Result] = []
    try:
        for n_vertices, n_edges in sizes:
            print(f"Start testing: graph V={n_vertices} E={n_edges}")
            seed = 209435 * n_vertices * n_edges

            def run_method(iter):
                main = Main(
                    Params(
                        seed=seed + iter,
                        n_vertices=n_vertices,
                        n_egdes=n_edges,
                        mst_root=0,
                    )
                )
                main.setup()

                try:
                    gc.disable()
                    start_time = time.monotonic()
                    mst(data_wrapper=main.data_wrapper)
                    end_time = time.monotonic()
                    return (end_time - start_time) * 1000.0
                finally:
                    gc.enable()

            times = [run_method(iter) for iter in range(repeat)]
            results.append(Result(
                n_vertices=n_vertices,
                n_edges=n_edges,
                runtimes_ms=times,
            ))
            print(f"Best time: {min(times)}ms")
    except KeyboardInterrupt:
        print("KeyboardInterrupt -- canceling...")

    return results


def plot_results(csv_path: pathlib.Path, which: str):
    output_plot_path = csv_path.parent.joinpath(f"{csv_path.name[0:-4]}-results.pdf")

    print(f"Plotting {csv_path.name} -> {output_plot_path.name}")
    data = np.genfromtxt(csv_path,
                         delimiter=",",
                         dtype=float,
                         skip_header=1)

    x_vertices = data[:, 0].astype(int)  # vertices
    x_edges = data[:, 1].astype(int)  # edges
    y = data[:, 2] / 1000.0  # runtime_ms

    fig, ax = plt.subplots(constrained_layout=True)

    if which == 'combined':
        def func(t, a, b):
            # for each vertex we have 1.5 edges
            vertices = np.sqrt(t / 3)
            edges = vertices * 3
            return a + b * np.log(vertices) * edges

        x_combined = (x_edges * x_vertices)
        curve_fit_result = optimize.curve_fit(func, x_combined, y)
        a, b = curve_fit_result[0]
        approx = func(x_combined, a, b)

        ax.plot(x_combined, y, '-', alpha=1.0, label="Time")
        ax.plot(x_combined, approx, '-', alpha=0.3,
                label="$\Theta(E * ln(V))$ approximation")

        def ticker_formatter(value, p):
            if value > 0:
                vertices = math.sqrt(value / 3)
                edges = vertices * 3
                return f"V={int(vertices / 1000)};E={int(edges / 1000)}"
            else:
                return ""

        ax.get_xaxis().set_major_formatter(
            matplotlib.ticker.FuncFormatter(ticker_formatter))

        for tick in ax.get_xticklabels():
            tick.set_rotation(45)

        ax.set_xlabel('Number of vertices and edges (thousands)')
        ax.set_ylabel('Time (sec)')
        ax.legend()

    else:
        raise Exception(f"Invalid value for 'which': {which}")

    fig.savefig(str(output_plot_path))


def main_performance_test_and_and_write_csv(repeat=5):
    results = main_performance_test(
        sizes=[
            (v * 100, v * 300) for v in range(1, 300)
        ],
        repeat=repeat,
    )
    with open('data/mst.csv', 'w') as output:
        times = ','.join([f"time{_}" for _ in range(repeat)])
        output.write(f"n_vertices,n_edges,avg,{times}\n")
        for r in results:
            runtime_ms = sum(r.runtimes_ms) / len(r.runtimes_ms)
            times = ','.join([str(_) for _ in r.runtimes_ms])
            output.write(f"{r.n_vertices},{r.n_edges},{runtime_ms},{times}\n")


if __name__ == '__main__':
    setup_logging(level=logging.INFO)
    # main_sample_from_book()

    main_performance_test_and_and_write_csv()
    plot_results(pathlib.Path('data/mst.csv'), which='combined')
