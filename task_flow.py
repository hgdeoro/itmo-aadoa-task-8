import dataclasses
import logging
import pathlib
import random
import sys
import time
import typing

import matplotlib
import matplotlib.pyplot as plt
import numpy as np

from itmo.fordfulkerson import FordFulkerson
from itmo.graph import Edge, Params, DataWrapper
from itmo.random_flow_graph import RandomFlowGraphGenerator


class Main:
    def __init__(self, params: Params):
        logging.debug("params=%s", params)
        self.params: Params = params
        self.data_wrapper: DataWrapper = None

    def _log_adjacency_list(self):
        logging.info("---------- Full adjacency list")
        assert self.data_wrapper.adjacency_list
        for v1 in range(0, self.data_wrapper.params.n_vertices):
            v2s = sorted(list(self.data_wrapper.adjacency_list[v1]))
            row_str = " ".join([str(_) for _ in v2s])
            print(f"[{v1:>3}] -> {row_str}")

    def setup(self):
        logging.info("---------- setup()")
        assert self.params is not None
        assert self.data_wrapper is None
        self.data_wrapper = DataWrapper(self.params)
        if self.params.edges:
            self.data_wrapper.load_graph_from_params()
        else:
            # self.data_wrapper.create_random_graph()
            raise NotImplementedError()
        self.data_wrapper.create_reverse_edges()
        self.data_wrapper.create_networkx_graph()
        # self._log_adjacency_list()


def setup_logging(level=logging.INFO):
    logging.basicConfig(level=level, stream=sys.stdout)
    logging.getLogger('matplotlib').setLevel(logging.WARNING)


def main_sample_graph():
    SOURCE = 4
    SINK = 5
    edges = [
        Edge(0, 2, 25.0),
        Edge(1, 3, 15.0),
        Edge(3, 0, 6.0),
        Edge(SOURCE, 0, 10.0),
        Edge(SOURCE, 1, 10.0),
        Edge(2, SINK, 10.0),
        Edge(3, SINK, 10.0),
    ]
    pos = {
        0: [-0.5, 0.5],
        1: [-0.5, -0.5],
        2: [0, 0.5],
        3: [0, -0.5],
        4: [-1, 0],
        5: [0.5, 0],
    }

    main = Main(
        Params(
            seed=1,
            n_vertices=0,
            n_egdes=0,
            edges=edges,
            source_vertex=SOURCE,
            sink_vertex=SINK,
            vis_pos=pos,
            vis_options=dict(
                node_size=600,
                width=0.5,
                font_size=14,
            )
        )
    )
    main.setup()
    main.data_wrapper.plot('data/0-graph-initial.pdf')

    after_iteration_callback = lambda augmenting_path: logging.info("ITER - augmenting_path: %s", augmenting_path)

    ford_fulkerson = FordFulkerson(main.data_wrapper,
                                   after_iteration_callback=after_iteration_callback)
    ford_fulkerson.run_method()
    main.data_wrapper.plot('data/1-graph-flow-optimized.pdf')


def main_sample_graph_2():
    SOURCE = 0
    SINK = 5
    edges = [
        Edge(SOURCE, 1, 16.0),
        Edge(SOURCE, 2, 13.0),
        Edge(1, 3, 12.0),
        Edge(2, 1, 4.0),
        Edge(3, 2, 9.0),
        Edge(2, 4, 14.0),
        Edge(4, 3, 7.0),
        Edge(3, SINK, 20.0),
        Edge(4, SINK, 4.0),
    ]
    pos = {
        1: [-0.5, 0.5],
        2: [-0.5, -0.5],
        3: [0, 0.5],
        4: [0, -0.5],
        SOURCE: [-1, 0],
        SINK: [0.5, 0],
    }

    main = Main(
        Params(
            seed=2,
            n_vertices=0,
            n_egdes=0,
            edges=edges,
            source_vertex=SOURCE,
            sink_vertex=SINK,
            vis_pos=pos,
            vis_options=dict(
                node_size=600,
                width=0.5,
                font_size=14,
            )
        )
    )
    main.setup()
    main.data_wrapper.plot(f'data/seed-{main.params.seed}-0-graph-initial.pdf')

    after_iteration_callback = lambda augmenting_path: logging.info("ITER - augmenting_path: %s", augmenting_path)

    ford_fulkerson = FordFulkerson(main.data_wrapper,
                                   after_iteration_callback=after_iteration_callback)
    ford_fulkerson.run_method()
    main.data_wrapper.plot(f'data/seed-{main.params.seed}-1-graph-flow-optimized.pdf')


def main_sample_graph_3():
    seed = 6836572
    random_graph_generator = RandomFlowGraphGenerator(
        random=random.Random(seed),
        rows=5,
        cols=10,
        edges_per_col=7,
    )
    random_graph_generator.generate()

    main = Main(
        Params(
            seed=6836572,
            n_vertices=random_graph_generator._n_vertices,
            n_egdes=0,
            edges=random_graph_generator.edges,
            source_vertex=random_graph_generator.source_vertex,
            sink_vertex=random_graph_generator.sink_vertex,
            vis_pos=random_graph_generator.pos,
            vis_options=dict(
                node_size=100,
                width=0.5,
                font_size=4,
            ),
            vis_edge_options=dict(
                font_size=4,
            )
        )
    )
    main.setup()
    main.data_wrapper.plot(f'data/seed-{main.params.seed}-0-graph-initial.pdf')

    ford_fulkerson = FordFulkerson(main.data_wrapper)
    ford_fulkerson.run_method()
    main.data_wrapper.plot(f'data/seed-{main.params.seed}-1-graph-flow-optimized.pdf')


@dataclasses.dataclass
class Result:
    size: typing.List[int]
    vertices: int
    edges: int
    runtimes_ms: typing.List[float]


def performance_test(sizes, repeat=5) -> typing.List[Result]:
    results: typing.List[Result] = []
    try:
        for rows, cols, edges_per_col in sizes:
            print(f"Start testing: graph ~{rows}x{cols}x{edges_per_col}")
            seed = 209435 * rows * cols
            random_graph_generator = RandomFlowGraphGenerator(
                random=random.Random(seed),
                rows=rows,
                cols=cols,
                edges_per_col=edges_per_col,
            )
            random_graph_generator.generate()

            main = Main(
                Params(
                    seed=seed * 2,
                    n_vertices=random_graph_generator._n_vertices,
                    n_egdes=0,
                    edges=random_graph_generator.edges,
                    source_vertex=random_graph_generator.source_vertex,
                    sink_vertex=random_graph_generator.sink_vertex,
                    # vis_pos=random_graph_generator.pos,
                    # vis_options=dict(
                    #     node_size=100,
                    #     width=0.5,
                    #     font_size=4,
                    # ),
                    # vis_edge_options=dict(
                    #     font_size=4,
                    # )
                )
            )
            main.setup()

            def run_method():
                ford_fulkerson = FordFulkerson(
                    main.data_wrapper,
                    rows * cols * 100,
                )
                start_time = time.monotonic()
                ford_fulkerson.run_method()
                return (time.monotonic() - start_time) * 1000.0

            times = [run_method() for _ in range(repeat)]
            # times.sort()
            # times = times[:-2]
            # runtime_ms = (sum(times) / len(times)) * 1000.0
            results.append(Result(
                size=[rows, cols, edges_per_col],
                runtimes_ms=times,
                vertices=main.params.n_vertices,
                edges=main.params.n_egdes,
            ))
            print(f"Best time: {min(times)}ms")
    except KeyboardInterrupt:
        print("KeyboardInterrupt -- canceling...")

    return results


def _func(v, e, a, b):
    return (a * v) * (b * e ** 2)


def plot_results(csv_path: pathlib.Path, which: str):
    assert which
    output_plot_path = csv_path.parent.joinpath(f"{csv_path.name[0:-4]}.pdf")

    print(f"Plotting {csv_path.name} -> {output_plot_path.name}")
    data = np.genfromtxt(csv_path,
                         delimiter=",",
                         dtype=float,
                         skip_header=1)

    x_vertices = data[:, 0].astype(int)  # vertices
    x_edges = data[:, 1].astype(int)  # edges
    y = data[:, 2] / 1000.0  # runtime_ms

    # polyfit = np.polyfit(x, y, deg=polyfit_deg)
    # polynomial = np.poly1d(polyfit)

    fig, ax = plt.subplots()

    if which == 'edges':
        polyfit = np.polyfit(x_edges, y, deg=1)
        polynomial = np.poly1d(polyfit)

        ax.plot(x_edges, y, '-', alpha=1.5, label="Time")
        ax.plot(x_edges, polynomial(x_edges), '-', alpha=0.3, label="Linear approximation")
        ax.set_xlabel('Edges (thousands)')
        ax.get_xaxis().set_major_formatter(
            matplotlib.ticker.FuncFormatter(lambda value, p: int(value / 1000.0)))

        ax.set_ylabel('Time (sec)')
        ax.set_xlim(xmin=0)
        ax.legend()
    elif which == 'vertices':
        polyfit = np.polyfit(x_vertices, y, deg=1)
        polynomial = np.poly1d(polyfit)

        ax.plot(x_vertices, y, '-', alpha=0.5, label="Time")
        ax.plot(x_vertices, polynomial(x_vertices), '-', alpha=0.3, label="Linear approximation")
        ax.get_xaxis().set_major_formatter(
            matplotlib.ticker.FuncFormatter(lambda value, p: int(value / 1000.0)))

        ax.set_xlabel('Vertices (thousands)')
        ax.set_ylabel('Time (sec)')
        ax.set_xlim(xmin=0)
        ax.legend()
    elif which == 'combined':
        x_combined = x_edges * x_vertices

        polyfit = np.polyfit(x_combined, y, deg=1)
        polynomial = np.poly1d(polyfit)

        ax.plot(x_combined, y, '-', alpha=0.5, label="Time")
        ax.plot(x_combined, polynomial(x_combined), '-', alpha=0.3, label="Linear approximation")
        ax.get_xaxis().set_major_formatter(
            matplotlib.ticker.FuncFormatter(lambda value, p: int(value / 1000000.0)))

        ax.set_xlabel('Combined $E*V$ (millions)')
        ax.set_ylabel('Time (sec)')
        ax.set_xlim(xmin=0)
        ax.legend()
    else:
        raise Exception(f"Invalid value for 'which': {which}")

    fig.savefig(str(output_plot_path))


if __name__ == '__main__':
    setup_logging(level=logging.INFO)

    main_sample_graph_3()
    sys.exit(0)


    repeat = 5

    # --------------------------------------------------
    # vertices constant, edges vary
    # --------------------------------------------------

    output_filename = 'data/results-ford_fulkerson-1.csv'
    # results: typing.List[Result] = performance_test(
    #     # [[30, 30, _] for _ in [100, 101]],
    #     [[50, 50, _] for _ in range(100, 1000, 100)],
    #     repeat=repeat
    # )
    #
    # print(results)
    #
    # with open(output_filename, 'w') as output:
    #     times = ','.join([f"time{_}" for _ in range(repeat)])
    #     output.write(f"vertices,edges,avg,{times}\n")
    #     for r in results:
    #         runtime_ms = sum(r.runtimes_ms) / len(r.runtimes_ms)
    #         times = ','.join([str(_) for _ in r.runtimes_ms])
    #         output.write(f"{r.vertices},{r.edges},{runtime_ms},{times}\n")

    plot_results(pathlib.Path(output_filename), 'edges')

    # --------------------------------------------------
    # vertices vary, edges constant
    # --------------------------------------------------

    output_filename = 'data/results-ford_fulkerson-2.csv'
    # results: typing.List[Result] = performance_test(
    #     # [[_, 30, 400] for _ in [30, 40]],
    #     [[_, 30, 400] for _ in range(30, 230, 10)],
    #     repeat=repeat
    # )
    #
    # print(results)
    #
    # with open(output_filename, 'w') as output:
    #     times = ','.join([f"time{_}" for _ in range(repeat)])
    #     output.write(f"vertices,edges,avg,{times}\n")
    #     for r in results:
    #         runtime_ms = sum(r.runtimes_ms) / len(r.runtimes_ms)
    #         times = ','.join([str(_) for _ in r.runtimes_ms])
    #         output.write(f"{r.vertices},{r.edges},{runtime_ms},{times}\n")

    plot_results(pathlib.Path(output_filename), 'vertices')

    # --------------------------------------------------
    # vertices vary, edges constant
    # --------------------------------------------------

    output_filename = 'data/results-ford_fulkerson-3.csv'
    results: typing.List[Result] = performance_test(
        [
            [30, 30, 300],
            [40, 30, 400],
            [50, 30, 500],
            [60, 30, 600],
            [70, 30, 700],
            [80, 30, 800],
            [90, 30, 900],
            [100, 30, 1000],
        ],
        repeat=repeat
    )
    
    print(results)
   
    with open(output_filename, 'w') as output:
        times = ','.join([f"time{_}" for _ in range(repeat)])
        output.write(f"vertices,edges,avg,{times}\n")
        for r in results:
            runtime_ms = sum(r.runtimes_ms) / len(r.runtimes_ms)
            times = ','.join([str(_) for _ in r.runtimes_ms])
            output.write(f"{r.vertices},{r.edges},{runtime_ms},{times}\n")

    plot_results(pathlib.Path(output_filename), 'combined')

    # --------------------------------------------------

    logging.info("Finished OK")
